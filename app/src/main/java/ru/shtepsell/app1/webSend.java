package ru.shtepsell.app1;

import android.os.AsyncTask;
import android.util.Log;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by shtepsell on 26.05.2015.
 */
public class webSend extends AsyncTask<String, Boolean, Boolean> {

    String LOG_TAG = "DEBUGME";
    String ur = "http://vds.spkud.ru/geo/";

    public webSend(){
    }

    @Override
    protected Boolean doInBackground (String... arg0){
        Log.d(LOG_TAG,arg0[0]);
        try {
            URL url = new URL(ur
                    + arg0[0]
            );
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            //conn.connect();
            Log.d(LOG_TAG, "RespCode: " + conn.getResponseCode() + " Resp: " + conn.getResponseMessage());
        } catch (Exception e) {
            Log.d(LOG_TAG, "Error[URL]: " + e.toString());
        }

        return null;
    }
}
