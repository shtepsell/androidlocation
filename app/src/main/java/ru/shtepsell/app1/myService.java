package ru.shtepsell.app1;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.StrictMode;

import android.telephony.TelephonyManager;
import android.util.Log;

/**
 * Created by Shteps on 07.05.2015.
 */
public class myService extends Service {
    PowerManager.WakeLock wakeLock;
    private LocationManager locationManager;
    String LOG_TAG = "DEBUGME";
    Location myLoc;

    public void onCreate() {
        super.onCreate();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        PowerManager pm = (PowerManager) getSystemService(this.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "DoNotSleep");
        Log.d(LOG_TAG, "onCreate");
    }
    @Override
    @Deprecated
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        Log.d(LOG_TAG, "onStart");
        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        if(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10000, 5, locationListener);
        else Log.d(LOG_TAG,"NETW disabled");
        if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 5, locationListener);
        else Log.d(LOG_TAG,"GPS disabled");

    }
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(LOG_TAG, "onStartCommand");
        //locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        return super.onStartCommand(intent, flags, startId);
    }
    public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "onDestroy");
    }
    public void onTaskRemoved(Intent rootIntent) {
        Log.d(LOG_TAG,"removed");
    }
    public IBinder onBind(Intent intent) {
        Log.d(LOG_TAG, "onBind");
        return null;
    }
    public LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            myLoc = location;
            Log.d(LOG_TAG," -> myLoc <-");
            if(myLoc==null) Log.d(LOG_TAG," -> myLoc is null");
            else {
                Log.d(LOG_TAG, " -> myLoc:: prov: " + myLoc.getProvider() + ", lt: " + myLoc.getLatitude());
                TelephonyManager phone = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
                new webSend().execute("?lt=" + myLoc.getLatitude()
                    + "&ln=" + myLoc.getLongitude()
                    + "&time=" + myLoc.getTime()
                    + "&imei=" + phone.getDeviceId().toString()
                    + "&accuracy=" + myLoc.getAccuracy()
                    + "&alt=" + myLoc.getAltitude()
                    + "&speed=" + myLoc.getSpeed()
                    + "&bearing=" + myLoc.getBearing()
                    + "&provider=" + myLoc.getProvider());
            }
        }

        @Override
        public void onProviderDisabled(String provider) {
            myLoc = locationManager.getLastKnownLocation(provider);
            Log.d(LOG_TAG," -> myLoc.provDis" + myLoc.getProvider());
        }

        @Override
        public void onProviderEnabled(String provider) {
            myLoc = locationManager.getLastKnownLocation(provider);
            Log.d(LOG_TAG," -> myLoc.provCh" + myLoc.getProvider());
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            myLoc = locationManager.getLastKnownLocation(provider);
            Log.d(LOG_TAG," -> myLoc.statusCh" + myLoc.getProvider());
        }
    };
}
