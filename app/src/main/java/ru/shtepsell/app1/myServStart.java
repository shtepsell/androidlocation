package ru.shtepsell.app1;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;


/**
 * Created by Shteps on 07.05.2015.
 */
public class myServStart extends BroadcastReceiver {
    final public static String ONE_TIME = "onetime";
    String LOG_TAG = "DEBUGME";
    @Override
    public void onReceive(Context context, Intent intent) {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "YOUR TAG");
        //Acquire the lock
        wl.acquire();
        try {
            Log.d(LOG_TAG, " TRYIT");
            Intent myServ = new Intent(context, myService.class);
            //context.stopService(myServ);
            context.startService(myServ);
        } catch (Exception e){
            Log.d(LOG_TAG," Error: " + e.toString());
        }
        //Release the lock
        wl.release();
    }
    public void SetAlarm(Context context)
    {
        AlarmManager am=(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, myServStart.class);
        intent.putExtra(ONE_TIME, Boolean.TRUE);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
        //After after 30 sec
        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * 30, pi);
    }
}
